# Libertine Tweak Tool

* xenial [![pipeline status](https://gitlab.com/doniks/libertine-tweak-tool/badges/xenial/pipeline.svg)](https://gitlab.com/doniks/libertine-tweak-tool/-/commits/xenial)
* focal [![pipeline status](https://gitlab.com/doniks/libertine-tweak-tool/badges/focal/pipeline.svg)](https://gitlab.com/doniks/libertine-tweak-tool/-/commits/focal)

Tweak and tune your desktop apps installed in Libertine

[Ubuntu Touch](https://ubuntu-touch.io/) comes with the [Libertine](http://docs.ubports.com/en/latest/userguide/dailyuse/libertine.html) tool to run
desktop applications on your mobile device. Libertine Tweak Tool allows you to tweak some of these desktop applications to work better on a small touch screen.

## Testing

### Stable version

To install the latest stable release you can use the [OpenStore](https://open-store.io/app/libertine-tweak-tool.doniks) directly on your device.

### Development version

To test the latest development version, you can get them from the gitlab website. Pick from xenial or focal depending on which version you want to test and from armhf or arm64 based on what device you have:

   * xenial, aka 16.04: [armhf](https://gitlab.com/doniks/libertine-tweak-tool/-/jobs/artifacts/xenial/browse/build/arm-linux-gnueabihf/app/?job=armhf_click), [arm64](https://gitlab.com/doniks/libertine-tweak-tool/-/jobs/artifacts/xenial/browse/build/aarch64-linux-gnu/app/?job=arm64_click)
   * focal, aka 20.04: [armhf](https://gitlab.com/doniks/libertine-tweak-tool/-/jobs/artifacts/focal/browse/build/arm-linux-gnueabihf/app/?job=armhf_click), [arm64](https://gitlab.com/doniks/libertine-tweak-tool/-/jobs/artifacts/focal/browse/build/aarch64-linux-gnu/app/?job=arm64_click)

Download the click - it should open with OpenStore and you can install it.

I hope these links work ok, otherwise you have to dig through the builds like [this](https://gitlab.com/doniks/libertine-tweak-tool/-/blob/xenial/docs/gitlab_download.webm).

## Development

```
git clone https://gitlab.com/doniks/libertine-tweak-tool
cd libertine-tweak-tool
clickable --arch arm64
clickable --arch armhf
clickable logs
```

### Desktop
To some degree it is possible to develop/test with `clickable desktop`. For this, note that clickable sets the home dir to:
`~/.clickable/home/`. So you might want to prepare this directory:
```
mkdir -p ~/.clickable/home/.local/share/libertine-container/user-data/xenial
mkdir -p ~/.clickable/home/.local/share/libertine-container/user-data/xenial/.config/transmission
touch ~/.clickable/home/.local/share/libertine-container/user-data/xenial/.config/transmission/settings.json
```

Then you can read/write the files there:
```
tail ~/.clickable/home/.bashrc
cd ~/.clickable/home/.local/share/libertine-container/user-data/xenial
cat .Xdefaults
cat .config/transmission/settings.json
cat .config/gtk-3.0/gtk.css
```

### Focal
```
git checkout focal
CLICKABLE_FRAMEWORK=ubuntu-sdk-20.04 clickable
```

### IDE
You can run `clickable ide`.

You can also quickly test qml changes without having to build the whole click everytime:
```
clickable ide bash
QML2_IMPORT_PATH=build/x86_64-linux-gnu/app/install/lib/x86_64-linux-gnu qmlscene qml/Main.qml
```

## Translation
I gladly welcome translations! You can follow these steps:
* clone this repository
* install poedit
* open the `po/xy.po` file (xy should be the language you are working on, e.g. de for German), or 
* if this language file doesn't exist yet open the `po/libertine-tweak-tool.doniks.pot` and create a new .po file
* translate the strings and save
* make a merge request with your updated .po file

## Feedback/Issues/Feature requests
Happy to hear your feedback or the tweaks that you apply. Find me in gitlab [issue tracker](https://gitlab.com/doniks/libertine-tweak-tool/issues), or on telegram: dohniks. Or post in the [forum thread](https://forums.ubports.com/topic/4806/libertine-tweak-tool).
