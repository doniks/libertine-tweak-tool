/*
 * Copyright (C) 2023  Peter Putz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * libertine-tweak-tool is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Lomiri.Components 1.3
//import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0

import Filewriter 1.0

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'libertine-tweak-tool.doniks'
    automaticOrientation: true

    width: units.gu(45)
    height: units.gu(75)
    property int itemheight: units.gu(6)
    property int itemspacing: units.gu(1)
    property alias container: container_id_text_field.text

    Shortcut {
        sequence: "Ctrl+Q"
        //        context: Qt.WindowShortcut // The shortcut is active when the window is active
        context: Qt.ApplicationShortcut // The shortcut is active when the application is active
        onActivated: {
            console.log('quit')
            Qt.quit()
        }
    }

    Settings{
        id: settings
    }


    function readSettings() {
        console.log("readSettings(", container, ")")
        var dpi = Filewriter.getXdpi(container)
        if (dpi !== -1){
            xdpi_setting.checked = true;
            xdpi_setting.value = dpi;
        } else {
            xdpi_setting.checked = false;
            xdpi_setting.value = 0;
        }

        gtkscroll_setting.checked = Filewriter.getGtkScroolbar(container)

        transmission_setting.checked = Filewriter.getTransmission(container)

        xterm_setting.checked = Filewriter.getXTermFont(container)

        bashrc_setting.checked = Filewriter.existsDotBashrc(container)

        lish_setting.checked = Filewriter.getLishPathExport()
    }



    Page {
        anchors.fill: parent

        header: PageHeader {
            id: pageHeader
            title: i18n.tr('Libertine Tweak Tool')
            flickable: mainFlickable
        }

        Flickable{
            id: mainFlickable
            anchors.fill: parent
            flickableDirection: Flickable.VerticalFlick
            Column{
                id: column
                spacing: itemspacing
                anchors{
                    left:parent.left
                    right: parent.right
                    leftMargin: itemspacing
                    rightMargin: itemspacing
                    topMargin: itemspacing
                    bottomMargin: itemspacing
                }







                // container
                Item {
                    width: parent.width
                    height: itemheight
                    Row{
                        spacing: itemspacing
                        anchors.top: parent.top
                        anchors.horizontalCenter: parent.horizontalCenter
                        Label{
                            id: container_id_label
                            anchors.verticalCenter: parent.verticalCenter
                            font.bold: true
                            text: i18n.tr("Container ID:")
                        }
                        TextField{
                            id: container_id_text_field
                            anchors.verticalCenter: parent.verticalCenter
                            inputMethodHints: Qt.ImhNoAutoUppercase
                            text: "focal"
                            onAccepted:{
                                readSettings()
                            }
                        }
                    }
                }













                // xdpi
                Setting {
                    width: parent.width
                    height: Math.max(itemheight,childrenRect.height)
                    id: xdpi_setting
                    property alias value: xdpi_slider.value
                    title:i18n.tr("Xdpi")
                    description:i18n.tr("Scale X applications by setting the DPI value, e.g. 220")
                    onCheckedChanged:{
                        // FIXME: at the start of the application, when we read the values from file, this triggers an oCC, thus a write!
                        console.log("Xdpi.oCC", checked)
                        if(checked && value === 0 )
                            value = 90
                        xdpi_write_timer.start()
                    }
                    Timer{
                        // use a timer before writing to the file, to avoid excessiv writes while the user is playing with the slider
                        id: xdpi_write_timer
                        interval: 1000
                        running: false
                        repeat: false
                        onTriggered:{
                            if (xdpi_setting.checked){
                                var dpi = Math.round(xdpi_setting.value)
                                Filewriter.setXdpi(container,dpi);
                                console.log("apply xdpi", dpi)
                            } else {
                                Filewriter.clearXdpi(container);
                                console.log("clear xdpi")
                            }
                        }
                        function start() {
                            // toggle it off and on, because that resets the elapsed time
                            xdpi_write_timer.running = false
                            xdpi_write_timer.running = true
                        }
                    }
                }


                // xdpi slider
                Item {
                    width: parent.width
                    height: itemheight - 3 * itemspacing
                    enabled: xdpi_setting.checked
                    Item{
                        anchors.left: parent.left
                        anchors.right: xdpi_input.left
                        anchors.verticalCenter: parent.verticalCenter
                        Slider {
                            id: xdpi_slider
                            anchors.left: parent.left
                            anchors.right: parent.right
                            anchors.rightMargin: column.spacing
                            anchors.verticalCenter: parent.verticalCenter
                            minimumValue: 96
                            maximumValue: 500
                            stepSize: 12
                            // https://www-archive.mozilla.org/unix/dpi.html
                            // "If possible, use a number that is a multiple of 6, or even 12, as numbers that aren't sometimes result in annoying rounding errors that cause adjacent bitmap font sizes to not increment and decrement linearly. "
                            onValueChanged:{
                                //console.log("SV -> trigger timer ", value, pressed)
                                // trigger the write timer
                                xdpi_write_timer.start()
                            }
                        }
                    }
                    TextField{
                        id: xdpi_input
                        anchors.right: parent.right
                        anchors.verticalCenter: parent.verticalCenter
                        width: units.gu(6)
                        // w 5 is too small in desktop
                        // w 9 is too large in desktop
                        // w 7 is still a bit too wide on deb
                        text: xdpi_slider.value.toFixed(0)
                        validator: IntValidator{bottom:0; top: 1000}
                        hasClearButton: false
                        inputMethodHints: Qt.ImhPreferNumbers
                        onAccepted:{
                            // Hit Enter key
                            //console.log("I", text);
                            xdpi_setting.value = Number(text);
                        }
                        onCursorVisibleChanged:{
                            if (cursorVisible){
                                // received focus
                                selectAll()
                            }else{
                                // lost focus
                                //console.log("ILF", text, xdpi_slider.value)
                                xdpi_slider.value = Number(text);
                            }
                        }
                    }
                }















                // gtk scrollbar
                Setting {
                    width: parent.width
                    height: Math.max(itemheight,childrenRect.height)
                    id: gtkscroll_setting
                    title: i18n.tr("Gtk scrollbar")
                    description:i18n.tr("Large scrollbar in gtk-3 applications")
                    onCheckedChanged:{
                        gtkcss_write_timer.start()
                    }
                    Timer{
                        id: gtkcss_write_timer
                        interval: 1000
                        running: false
                        repeat: false
                        onTriggered:{
                            if (gtkscroll_setting.checked){
                                Filewriter.setGtkScroolbar(container)
                                console.log("apply gtk scrollbar")
                            } else {
                                Filewriter.clearGtkScroolbar(container)
                                console.log("reset gtk scrollbar")
                            }
                        }
                        function start() {
                            // toggle it off and on, because that resets the elapsed time
                            gtkcss_write_timer.running = false
                            gtkcss_write_timer.running = true
                        }
                    }
                }





















                // transmission
                Setting {
                    width: parent.width
                    height: Math.max(itemheight,childrenRect.height)
                    id: transmission_setting
                    title:i18n.tr("Transmission")
                    description:i18n.tr("Control the torrent client <i>transmission</i> via its <a href='http://localhost:9091' >web interface</a>. Also downloads will start automatically when a torrent file is placed in the Downloads folder. (Note: Transmission must have run at least once to initialize settings and it must be closed to change the settings here.)")
                    onCheckedChanged:{
                        transmission_write_timer.start()
                    }
                    Timer{
                        id: transmission_write_timer
                        interval: 1000
                        running: false
                        repeat: false
                        onTriggered:{
                            if (transmission_setting.checked){
                                Filewriter.setTransmission(container);
                                console.log("apply transmission")
                            } else {
                                Filewriter.clearTransmission(container);
                                console.log("clear transmission")
                            }
                        }
                        function start() {
                            // toggle it off and on, because that resets the elapsed time
                            transmission_write_timer.running = false
                            transmission_write_timer.running = true
                        }
                    }
                }














                // xterm
                Setting {
                    width: parent.width
                    height: Math.max(itemheight,childrenRect.height)
                    id: xterm_setting
                    title:i18n.tr("XTerm")
                    description:i18n.tr("Set XTerm font to DejaVu Sans Mono, 10")
                    onCheckedChanged:{
                        console.log("XTerm", checked)
                        if(checked)
                            Filewriter.setXTermFont(container);
                        else
                            Filewriter.clearXTermFont(container);
                    }
                }







                // lish
                Setting {
                    width: parent.width
                    height: Math.max(itemheight,childrenRect.height)
                    id: lish_setting
                    title:i18n.tr("Libertine Shell - lish")
                    description:i18n.tr("<i>Lish</i> is a small command line tool to enter the container. It prefixes the prompt with the container name, e.g., <i>(xenial)phablet@ubuntu-phablet:~$</i>. <i>lirsh</i> is a second tool that opens a \"root\" shell. Both are included in <a href='https://gitlab.com/doniks/libertine-tweak-tool/-/tree/master_new/bin/' >LTT</a>. This setting gives easy access to these tools by extending the <code>PATH</code> variable in your <code>.bashrc</code> file. Note that this is a global setting and independent of the selected libertine container. You have to reopen the Terminal app for this to take effect.")
                    onCheckedChanged:{
                        console.log("lish", checked)
                        if(checked)
                            Filewriter.setLishPathExport();
                        else
                            Filewriter.clearLishPathExport();
                    }
                }








                // bashrc
                Setting {
                    width: parent.width
                    height: Math.max(itemheight,childrenRect.height)
                    id: bashrc_setting
                    title:i18n.tr("Bashrc")
                    description:i18n.tr("Copy your global .bashrc file into the container. That way you get the usual terminal experience with colors, autocompletion, aliases, etc.")
                    onCheckedChanged:{
                        console.log("bashrc", checked)
                        if(checked)
                            Filewriter.writeDotBashrc(container);
                        else
                            Filewriter.deleteDotBashrc(container);
                    }
                }







            }
        }

    }


    Component.onCompleted:{
        readSettings()
    }
}
