#ifndef FILEWRITER_H
#define FILEWRITER_H

#include <QObject>
#include <QStandardPaths>

class Filewriter: public QObject {
    Q_OBJECT

protected:
    QString libertineRootDir;
    QString libertineUserDir;
    QString xdefaults;
    QString gtkcssdir;
    QString gtkfilename;

public:
    Filewriter();
    ~Filewriter() = default;

    Q_INVOKABLE QStringList listContainers();

    Q_INVOKABLE int getXdpi(QString container);
    void setXdpi(QString container, bool clear, uint dpi);
    Q_INVOKABLE void setXdpi(QString container, uint dpi);
    Q_INVOKABLE void clearXdpi(QString container);
    QString filenameXdefaults(QString container);

    Q_INVOKABLE bool getGtkScroolbar(QString container);
    Q_INVOKABLE void setGtkScroolbar(QString container);
    Q_INVOKABLE void clearGtkScroolbar(QString container);
    QString filenameGtkCSS(QString container);

    void setTransmission(QString container, bool enable);
    Q_INVOKABLE bool getTransmission(QString container);
    Q_INVOKABLE void setTransmission(QString container);
    Q_INVOKABLE void clearTransmission(QString container);
    QString filenameTransmission(QString container);

    Q_INVOKABLE bool getXTermFont(QString container);
    void setXTermFont(QString container, bool clear, QString font, uint size);
    Q_INVOKABLE void setXTermFont(QString container);
    Q_INVOKABLE void clearXTermFont(QString container);

    Q_INVOKABLE QString filenameDotBashrc(QString container);
    Q_INVOKABLE bool existsDotBashrc(QString container);
    Q_INVOKABLE void writeDotBashrc(QString container);
    Q_INVOKABLE void deleteDotBashrc(QString container);

    Q_INVOKABLE QString filenameHomeDotBashrc();
    Q_INVOKABLE bool getLishPathExport();
    Q_INVOKABLE void setLishPathExport();
    Q_INVOKABLE void clearLishPathExport();
    Q_INVOKABLE void clearLegacyLishPathExport();

    Q_INVOKABLE QString configPath();
    Q_INVOKABLE QString cachePath();
    Q_INVOKABLE QString appDataPath();

    Q_INVOKABLE void speak();

};


#endif
